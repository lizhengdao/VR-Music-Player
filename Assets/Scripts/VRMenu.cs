﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRMenu : MonoBehaviour
{
   public Transform viewTransform;

   public void resetRotation()
   {
      transform.rotation = Quaternion.Euler(0,0,0);
   }
   public void showMenu()
   {
      gameObject.transform.rotation = Quaternion.Euler(0, viewTransform.eulerAngles.y, 0);
      gameObject.SetActive(true);

   }
   
   public void hideMenu()
   {
      resetRotation();      
      gameObject.SetActive(false);
   }
}

