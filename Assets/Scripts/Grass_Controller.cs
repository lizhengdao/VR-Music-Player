﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass_Controller : MonoBehaviour
{
    private bool forward = true;
    private Material water;
    private Transform[] trees;
    private Transform[] grass;
    private Spectometer spectometer;
    private float time = 0;
    private float[] audioData;

    // Start is called before the first frame update
    void Start()
    {
        LoadAll();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        audioData = spectometer.GetBuffer();

        if (time >= 7.0)
        {
            time = 0.0f;
            UpdateWater();
            UpdateWind();
        }
    }

    void UpdateWind()
    {
        foreach (Transform t in trees)
        {
            float h = 29 - (29 * (1 - audioData[5]));
            t.GetComponent<Renderer>().sharedMaterial.SetFloat("_MBAmplitude", 1f + h);
            h = 5 - (5 * (1 - audioData[4]));
            t.GetComponent<Renderer>().sharedMaterial.SetFloat("_MBFrequency", 1f + h);
        }
        
        foreach (Transform g in grass)
        {
            float h = 9 - (9 * (1 - audioData[5]));
            g.GetComponent<Renderer>().sharedMaterial.SetFloat("_MBAmplitude", 1f + h);
            h = 1 - (1 * (1 - audioData[4]));
            g.GetComponent<Renderer>().sharedMaterial.SetFloat("_MBFrequency", 1f + h);
        }
    }
    
    void UpdateWater()
    {
        float h = 120 - (120 * audioData[6]);
        water.SetFloat("speed", 30f + h);
        
        h = .005f - (.005f * audioData[7]);
        water.SetFloat("displacement", .005f + h);
    }

    void LoadAll()
    {
        UnityEngine.SceneManagement.Scene activeScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        GameObject[] rootObjects = activeScene.GetRootGameObjects();

        foreach (var o in rootObjects)
        {
            if (o.name == "Water")
            {
                water = o.GetComponentInChildren<Renderer>().sharedMaterial;
            }
            
            if (o.name == "Trees")
            {
                trees = new Transform[o.transform.childCount];
                int helper = 0;
                foreach (Transform child in o.transform)
                {
                    trees[helper] = child;
                    helper++;
                }
            } 
            
            if (o.name == "Grass")
            {
                grass = new Transform[o.transform.childCount];
                int helper = 0;
                foreach (Transform child in o.transform)
                {
                    grass[helper] = child;
                    helper++;
                }
            }

            if (o.name == "AudioPlayer")
            {
                spectometer = o.GetComponent<Spectometer>();
            }
        }
    }
}
