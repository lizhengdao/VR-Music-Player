﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMenu : MonoBehaviour
{
    public Text artist;
    public Text title;
    public Button playPause;
    public Button shuffle;
    
    // Sprites for play and pause action
    public Sprite playIdle;
    public Sprite playHover;
    public Sprite pauseIdle;
    public Sprite pauseHover;

    // Start is called before the first frame update
    void Start()
    {
               
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateSongInfo(string artist, string title)
    {
        this.artist.text = artist;
        this.title.text = title;
    }

    public void UpdatePlayPauseBtn(bool paused)
    {
        playPause.image.sprite = paused ? playIdle : pauseIdle;
        SpriteState tempState = playPause.spriteState;
        tempState.highlightedSprite = paused ? playHover : pauseHover;
        playPause.spriteState = tempState;
    }

    public void UpdateShuffleBtn(bool shuffle)
    {
        this.shuffle.image.color = shuffle ? Color.cyan : Color.white;
    }
}
