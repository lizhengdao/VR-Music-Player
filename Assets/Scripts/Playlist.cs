﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class Playlist : MonoBehaviour
{
    public string playlistName;
    
    private AudioClip[] clipsOriginal;
    private AudioClip[] clipsShuffle;
    
    private AudioSource source;
    private int index = 0;
    private bool shuffle = false;
    private bool paused = false;

    // Information about current song
    public string title;
    public string artist;
    
    private PlayerMenu playerMenu;

    // Start is called before the first frame update
    void Start()
    {
        FindMenu();
        LoadSongs();
        // plays first song
        PlaySong();
    }
    
    void Update()
    {
        // only for debugging
        if (Input.GetKeyDown("n"))
        {
            PreviousSong();
        }

        if (Input.GetKeyDown("m"))
        {
            NextSong();
        }
        
        if (Input.GetKeyDown("p"))
        {
            PauseSong();
        }
        
        if (Input.GetKeyDown("s"))
        {
            ToggleShuffle();
        }
    }
    
    private void FindMenu()
    {
        UnityEngine.SceneManagement.Scene activeScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        GameObject[] rootObjects = activeScene.GetRootGameObjects();

        foreach (var o in rootObjects)
        {
            if (o.name == "Menu")
            {
                playerMenu = o.GetComponent<PlayerMenu>();
            }
        }
    }

    /**
     * returns upcoming clip in queue
     */
    private AudioClip GetClip()
    {
        if (shuffle)
        {
            return clipsShuffle[index];
        }
        return clipsOriginal[index];
    }

    /**
     * toggles if songs are shuffled
     */
    public void ToggleShuffle()
    {
        if (shuffle == false)
        {
            ShuffleSongs();
            shuffle = true;
        }
        else
        {
            shuffle = false;
        }

        playerMenu.UpdateShuffleBtn(shuffle);
    }
    
    /**
     * plays next song
     */
    public void NextSong()
    {
        index++;
        if (index >= clipsOriginal.Length)
        {
            index = 0;
        }
        
        PlaySong();
    }

    /**
     * plays previous song
     */
    public void PreviousSong()
    {
        index--;
        if (index < 0)
        {
            index = clipsOriginal.Length - 1;
        }
        
        PlaySong();
    }
    
    /**
     * toggles pause of music
     */
    public void PauseSong()
    {
        if (paused)
        {
            source.UnPause();
            paused = false;
        }
        else
        {
            source.Pause();
            paused = true;
        }
        playerMenu.UpdatePlayPauseBtn(paused);
    }

    /**
     * helper function for NextSong and PreviousSong
     */
    private void PlaySong()
    {
        source.clip = GetClip();
        source.Play();
        LogInformation();
        paused = false;
        playerMenu.UpdatePlayPauseBtn(paused);

        StartCoroutine(WaitForTrackToEnd());
    }

    /**
     * plays next song if current playing song ends and playlist is not paused
     */
    private IEnumerator WaitForTrackToEnd()
    {
        while (source.isPlaying || paused)
        {

            yield return new WaitForSeconds(0.01f);
        }
        NextSong();
    }

    /**
     * loads all songs from playlist file
     */
    private void LoadSongs()
    {
        source = gameObject.GetComponent<AudioSource>();
        clipsOriginal = Resources.LoadAll<AudioClip>(playlistName);
    }
    
    /**
     * creates a shuffled version of the playlist
     */
    private void ShuffleSongs()
    {
        clipsShuffle = clipsOriginal;
        Random rnd = new Random();
        
        for (int i = 0; i < clipsShuffle.Length; i++)
        {
            AudioClip temp = clipsShuffle[i];
            int objIndex = rnd.Next(clipsShuffle.Length);
            clipsShuffle[i] = clipsShuffle[objIndex];
            clipsShuffle[objIndex] = temp;
        }
    }
    
    /**
     * logs information about the current song playing
     */
    private void LogInformation()
    {
        string helper = source.clip.name;
        string[] splitString = helper.Split(new string[] {"-"}, StringSplitOptions.None);
        
        artist = splitString[0];
        title = splitString[1];
        
        playerMenu.UpdateSongInfo(artist, title);
    }
}
