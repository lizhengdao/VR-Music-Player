﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Authentication.ExtendedProtection;
using UnityEngine;

public class Spectometer : MonoBehaviour
{
    // defines how many samples will be taken from the spectrum
    public static int samplesNum = 512;
    // defines number of frequency bands
    private static int bandNum = 8;
    
    private AudioSource source;
    
    // for testing purpose only
    private GameObject[] bandCubes = new GameObject[bandNum];
    // private Material[] bandCubeMat = new Material[8];
    //public Material mat;
    
    private float[] samplesLeft = new float[samplesNum];
    private float[] samplesRight = new float[samplesNum];

    // frequency band and buffer
    private float[] freqBands = new float[bandNum];
    private float[] freqBuffer = new float[bandNum];
    private float[] bufferDecreas = new float [bandNum];

    private float[] freqBandHighest = new float[bandNum];

    // audio bands transform values from frequency bands into values from 0 to 1
    public float[] audioBand, audioBuffer;
    
    // current amplitude and amplitude buffer
    public float Amplitude, AmplitudeBuffer;
    private float AmplitudeHighest;

    // current loudness
    private float Loudness, LoudnessBufferDecrease, LoudnessHighest, LoudnessBuffer;
    private float currentUpdateTime = 0f;
    // loudness values 0 to 1
    public float audioLoudness, audioLoudnessBuffer;

    public float[] GetBuffer()
    {
        return audioBuffer;
    }

    public enum channel
    {
        Stereo,
        Left,
        Right
    };

    public channel Channel = new channel();

    // public float initalAudioProfile;

    void Start()
    {
        audioBand = new float[bandNum];
        audioBuffer = new float[bandNum];
        
        // for testing only
        // CreateVisuals();

        source = gameObject.GetComponent<AudioSource>();
        // AudioProfile(initalAudioProfile);
        
        /*
        Debug.Log(source.clip.ambisonic);
        Debug.Log(source.clip.channels);
        Debug.Log(source.clip.frequency);
        Debug.Log(source.clip.length);
        Debug.Log(source.clip.samples);
        */

        // Debug.Log(AudioSettings.GetConfiguration().sampleRate);
    }

    /**
     * it might happen that the visualisation looks strange in the beginning. This method can help.
     */
    private void AudioProfile(float initialAudioProfile)
    {
        for (int i = 0; i < bandNum; i++)
        {
            freqBandHighest[i] = initialAudioProfile;
        }
    }

    void Update()
    {
        GetSpectrumAudioSource();
        MakeFrequancyBands();
        BandBuffer();
        CreateAudioBands();
        GetAmplitude();
        GetLoudness();
        
        // VisualiseCubes();
    }

    /**
     * gets the spectrum data from the AudioSource
     */
    private void GetSpectrumAudioSource()
    {
        source.GetSpectrumData(samplesLeft, 0, FFTWindow.BlackmanHarris);
        source.GetSpectrumData(samplesRight, 1, FFTWindow.BlackmanHarris);
    }

    /**
     * create frequency bands. Each band contains the following frequencies
     *
     * Band 1: 0-86Hz
     * Band 2: 86-259Hz
     * Band 3: 259-602Hz
     * Band 4: 602-1290
     * Band 5: 1290-2666
     * Band 6: 2666-5418
     * Band 7: 5418-10922
     * Band 8: 10922-21930
     */
    private void MakeFrequancyBands()
    {
        int count = 0;

        for (int i = 0; i < bandNum; i++)
        {
            float avg = 0;
            int toAdd = (int) Mathf.Pow(2, i) * 2;

            if (i == bandNum - 1)
            {
                toAdd += 2;
            }

            for (int j = 0; j < toAdd; j++)
            {
                if (Channel == channel.Stereo)
                {
                    avg += (samplesLeft[count] + samplesRight[count]) * (count + 1);
                } else if (Channel == channel.Left)
                {
                    avg += samplesLeft[count] * (count + 1);
                } else if (Channel == channel.Right)
                {
                    avg += samplesRight[count] * (count + 1);
                }
                
                count++;
            }

            avg /= count;

            freqBands[i] = avg * 10;
        }
    }

    /**
     * the buffer makes changes in the bands more settle and allows for a smoother transition between values
     */
    private void BandBuffer()
    {
        for (int i = 0; i < bandNum; i++)
        {
            if (freqBands[i] > freqBuffer[i])
            {
                freqBuffer[i] = freqBands[i];
                bufferDecreas[i] = 0.0005F;
            }
            if (freqBands[i] < freqBuffer[i])
            {
                freqBuffer[i] -= bufferDecreas[i];
                bufferDecreas[i] *= 1.2F;
            }
        }
    }
    
    /**
     * transforms values from frequency bands into values between 0 and 1 
     */
    private void CreateAudioBands()
    {
        for (int i = 0; i < bandNum; i++)
        {
            if (freqBands[i] > freqBandHighest[i])
            {
                freqBandHighest[i] = freqBands[i];
            }
            audioBand[i] = (freqBands[i] / freqBandHighest[i]);
            audioBuffer[i] = (freqBuffer[i] / freqBandHighest[i]);
        }
    }

    /**
     * calculates current amplitude
     */
    private void GetAmplitude()
    {
        float tempAmplitude = 0;
        float tempAmplitudeBuffer = 0;
        
        for (int i = 0; i < bandNum; i++)
        {
            tempAmplitude += audioBand[i];
            tempAmplitudeBuffer += audioBuffer[0];
        }

        if (tempAmplitude > AmplitudeHighest)
        {
            AmplitudeHighest = tempAmplitude;
        }

        Amplitude = tempAmplitude / AmplitudeHighest;
        AmplitudeBuffer = tempAmplitudeBuffer / AmplitudeHighest;
    }

    /**
     * gets the current loudness (measure every 100ms)
     */
    private void GetLoudness()
    {
        currentUpdateTime += Time.deltaTime;

        if (currentUpdateTime >= 0.1f)
        {
            currentUpdateTime = 0f;
            Loudness = 0f;

            for (int i = 0; i < samplesNum; i++)
            {
                if (Channel == channel.Stereo)
                {
                    Loudness += Mathf.Abs(samplesLeft[i] + samplesRight[i]);
                } else if (Channel == channel.Left)
                {
                    Loudness += Mathf.Abs(samplesLeft[i]);
                } else if (Channel == channel.Right)
                {
                    Loudness += Mathf.Abs(samplesRight[i]);
                }
            }

            Loudness /= samplesNum;

            if (Loudness > LoudnessBuffer)
            {
                LoudnessBuffer = Loudness;
                LoudnessBufferDecrease = 0.0005F;
            }
            if (Loudness < LoudnessBuffer)
            {
                LoudnessBuffer -= LoudnessBufferDecrease;
                LoudnessBufferDecrease *= 1.2F;
            }
            
            if (Loudness > LoudnessHighest)
            {
                LoudnessHighest = Loudness;
            }
            
            audioLoudness = Mathf.Abs(Loudness / LoudnessHighest);
            audioLoudnessBuffer = Mathf.Abs(LoudnessBuffer / LoudnessHighest);
        }
    }

    /**
     * creates band for testing
     */
    private void CreateVisuals()
    {
        int offset = (bandNum / 2) * -1;
        offset -= 2;
        
        for (int i = 0; i < bandCubes.Length; i++)
        {
            bandCubes[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
            int number = i + 1;
            bandCubes[i].name = "bandCube" + number;
            bandCubes[i].transform.localPosition += new Vector3(offset, 0, 1);
            offset += 2;
            // bandCubes[i].GetComponent<Renderer>().material = mat;
            // bandCubeMat[i] = bandCubes[i].GetComponent<MeshRenderer>().materials[0];
        }
    }

    /**
     * visualises changes for testing
     */
    private void VisualiseCubes()
    {
        for (int i = 0; i < bandCubes.Length; i++)
        {
            bandCubes[i].transform.localScale = new Vector3(1, freqBuffer[i] + 1, 1);
            bandCubes[i].transform.localPosition = new Vector3(bandCubes[i].transform.localPosition.x, (freqBuffer[i] + 1) / 2, 1);
//            bandCubeMat[i].SetColor("_EmissionColor", new Color(audioBuffer[i], audioBuffer[i], audioBuffer[i]));
        }
    }
}
